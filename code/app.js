
// Get the modal
var modCat = document.getElementById("modCat");
var modDog = document.getElementById("modDog");
var modHor = document.getElementById("modHor");
var modSna = document.getElementById("modSna");

// Get the button that opens the modal
var btnCat = document.getElementById("btnCat");
var btnDog = document.getElementById("btnDog");
var btnHor = document.getElementById("btnHor");
var btnSna = document.getElementById("btnSna");

// Get the <span> element that closes the modal
var spanCat = document.getElementsByClassName("close")[0];
var spanDog = document.getElementsByClassName("close")[1];
var spanHor = document.getElementsByClassName("close")[2];
var spanSna = document.getElementsByClassName("close")[3];

// Store JSON information in here
var factsCat = null;
var factsDog = null;
var factsHor = null;
var factsSna = null;

function load(){
	
	//Get the id of the divs from HTML
	var idCat = 'displayCatFacts';
	var idDog = 'displayDogFacts';
	var idHor = 'displayHorseFacts';
	var idSna = 'displaySnailFacts';
	
	//Read JSON file and populate the data in the Modals
	readTextFile("api/facts/cat.json",function(response){
		factsCat = JSON.parse(response);
		console.log(factsCat);
		//Parameters are: 1) Where we store the JSON informaton 2)The div id in the html
		populateData(factsCat,idCat);
	});
	
	readTextFile("api/facts/dog.json",function(response){
		factsDog = JSON.parse(response);
		console.log(factsDog)
		populateData(factsDog,idDog);
	});
	
	readTextFile("api/facts/horse.json",function(response){
		factsHor = JSON.parse(response);
		console.log(factsHor)
		populateData(factsHor,idHor);
	});
	
	readTextFile("api/facts/snail.json",function(response){
		factsSna = JSON.parse(response);
		console.log(factsSna)
		populateData(factsSna,idSna);
	});	
}

function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText, callback);
        }
    }
    rawFile.send(null);
}

function populateData(animalFacts, divId){
	for (var i = 0; i < animalFacts.all.length; i++)
	{
		var catFact = animalFacts.all[i];
		var factText = catFact.text;
		var factUser;
		if (catFact.user == null)
		{
			factUser = "Anonymous";
		}
		else
		{
			factUser = catFact.user.name.first + " " + catFact.user.name.last;
		}
		var html = "<div class=\"animal-infos\"><p class=\"fact-text\">";
		html += factText;
		html += "</p><p class=\"fact-user\">";
		html += factUser;
		html += "</p></div>";
		document.getElementById(divId).innerHTML += html;
	}
}

// When the user clicks on the button, open the modal

btnCat.onclick = function() {
	modCat.style.display = "block";
}

btnDog.onclick = function() {
	modDog.style.display = "block";
}
btnHor.onclick = function() {
	modHor.style.display = "block";
}
btnSna.onclick = function() {
	modSna.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
spanCat.onclick = function() {
	modCat.style.display = "none";
}

spanDog.onclick = function() {
	modDog.style.display = "none";
}

spanHor.onclick = function() {
	modHor.style.display = "none";
}

spanSna.onclick = function() {
	modSna.style.display = "none";
}

